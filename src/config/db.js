const { username, password } = require('../../config')

module.exports = {
    mongoURI: `mongodb+srv://${username}:${password}@cluster0-ckd17.mongodb.net/test?retryWrites=true`
}