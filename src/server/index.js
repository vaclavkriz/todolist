const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const todos = require('../routes/todos');
//const users = require('../routes/users');

const app = express();
// body parser middleware
app.use(bodyParser.urlencoded({ extended: false })); // Parses urlencoded bodies
app.use(bodyParser.json()); // Send JSON responses


//use routes
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
});

//DB config
const db = require('../config/db').mongoURI;

app.use('/todos', todos);
//app.use('/users', users)

//listen to server
const port = process.env.PORT || 5000;

//connect to mongo
mongoose
    .connect(db, { useNewUrlParser: true })
    .then(() => {
        app.listen(port, () => {
            console.log(`Server started on ${port}`)
        });
        console.log('Mongo connected')
    })
    .catch(err => console.log(err));


