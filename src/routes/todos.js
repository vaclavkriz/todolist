const express = require('express');
const router = express.Router();
const Todos = require('../modals/todos');
const uuid = require('uuid');

router.get('/', (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    Todos.find()
        .sort({ date: -1 })
        .then(items => {
            res.json(items);
        })
})

router.get('/find/:id', (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    Todos.find({
        'name': { "$regex": req.params.id, "$options": "i" }
    })
        .sort({ date: -1 })
        .then(resp => {
            res.json(resp);
        })
})

router.post('/add', (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    const { name, description, done, date, importance } = req.body;
    console.table(req.body);

    Todos.findOne({ name: name })
        .then(todo => {
            console.log(todo)
            if (todo) {
                res.json({ message: "This todoname already exists" })
            }
            else {
                const newTodo = new Todos({
                    name,
                    description,
                    done: false,
                    date,
                    importance
                });
                newTodo.save()
                    .then(item => {
                        res.json({ message: `Todo ${item.name} has been successfully added!` })
                    })
                    .catch(err => {
                        console.log(err)
                        res.json({ message: 'Internal error' })
                    })
            }
        })
})

router.post('/update', (req, res) => {
    const { id, name, description, done, date, importance } = req.body;
    res.setHeader('Access-Control-Allow-Origin', '*');
    if (id) {
        Todos.findOneAndUpdate({ '_id': id }, { name, description, done, date, importance })
            .then(resp => {
                if (resp) {
                    res.json({
                        message: "Todo has been successfully updated"
                    })  
                }
                else {
                    res.json({
                        message: "Wrong data"
                    })
                }
                
            })
            .catch(err => {
                res.json({ message: "err" })
            })
    }
    else {
        res.json({ message: "error" })
    }

})

router.post('/setDone', (req, res) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    const { id, done } = req.body;
    if (id) {
        Todos.findOneAndUpdate({ '_id': id }, { done: !done }).then(resp => {
            if (resp) {
                res.json({ message: "Successfully changed the state of todo" })
            }
            else {
                res.json({ message: "Internal error" })
            }
        })
    } else {
        res.json({ message: "Internal error" })
    }
})

router.post('/remove', (req, res) => {
    const { id } = req.body;
    if (id) {
        Todos.findOneAndDelete({ '_id': id })
            .then(resp => {
                if (resp) {
                    res.json({ message: 'This todo was successfully removed!' })
                }
                else {
                    res.json({ message: 'Todo cannot be removed' })
                }
            })
            .catch(err => res.status(404).json({ message: 'Internal error' }));
    } else {
        res.json({ message: "Internal error" })
    }

});


router.delete('/removeAll', (req, res) => {
    Todos.deleteMany({}).then(resp => {
        res.json({ success: true })
    })
        .catch(err => {
            res.json({ message: "err" })
        })
})

module.exports = router;