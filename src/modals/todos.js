const mongoose = require('mongoose');

const TodosSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false,
        default: "",
    },
    done: {
        type: Boolean,
        required: false,
        default: false
    },
    date: {
        type: Number,
        required: true
    },
    importance: {
        type: Number,
        required: false,
        default: 1
    },
});

const Todos = mongoose.model('Todos', TodosSchema);

module.exports = Todos;
